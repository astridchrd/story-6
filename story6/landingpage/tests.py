from django.test import TestCase,Client
from django.urls import resolve
from . import views
from django.http import HttpRequest
from .models import Status
from .forms import StatusForm
from datetime import datetime
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time



c= Client()

class UnitTestLandingPage(TestCase):
    def test_ada_url_status(self):
        response = c.get('/status/')
        self.assertEqual(response.status_code, 200)
    
    def test_status_pakai_status_page(self):
        response = c.get('/status/')
        self.assertTemplateUsed(response, 'status.html')
    
    def test_status_panggil_fungsi_views_status(self):
        found = resolve('/status/')
        self.assertEqual(found.func, views.StatusPost)

    def test_ada_kalimat_halo(self):
        response = c.get("/status/")
        case = "Halo, Apa Kabar?"
        content = response.content.decode("utf8")
        self.assertIn(case, content)

    def test_ada_tombol_update_status(self):
        response = c.get("/status/")
        content = response.content.decode("utf8")
        case = '<button  class="btn text buttonUpdate" type="submit">Update Status'
        self.assertIn(case, content)
    
    def test_ada_form(self):
        response = c.get("/status/")
        content = response.content.decode("utf8")
        case1 = '<label>Status: '

    def test_apakah_ada_table(self):
        response = c.get('/status/')
        content = response.content.decode('utf8')
        self.assertIn("<table", content)

    def test_input_form(self):
        case = 'test'
        form_data={'status':case}
        form = StatusForm(data = form_data)
        self.assertTrue(form.is_valid())

    def test_bisa_membuat_status_baru(self):
        new_status =Status.objects.create(status='senang senang',tanggal=timezone.now())
        jumlah_objek = Status.objects.all().count()
        self.assertEqual(jumlah_objek,1)

    def test_form_validation_for_blank_items(self):
        form = StatusForm(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )
    
class FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        #chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_post(self):
        self.browser.get('http://127.0.0.1:8000/status/')
        self.assertIn('Astrid Chaerida', self.browser.title)

        text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Halo, Apa Kabar?', text)

        inputbox1 = self.browser.find_element_by_id('id_status')
        time.sleep(1)
        inputbox1.send_keys('Halo')
        time.sleep(2)

        self.assertIn('Halo', self.browser.page_source)
        
