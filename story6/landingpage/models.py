from django.db import models

class Status(models.Model):
    status = models.CharField(max_length=300)
    tanggal = models.DateTimeField(auto_now=True)

    def __str__(self):
        self.status
